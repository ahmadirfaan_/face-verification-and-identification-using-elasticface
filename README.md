## This is the official repository of the paper:
#### ElasticFace: Elastic Margin Loss for Deep Face Recognition
Paper on arxiv: [arxiv](https://arxiv.org/pdf/2109.09416.pdf)

<b>This repository has been modified by [Ahmad Irfan](https://gitlab.com/ahmadirfaan_) for Face Recognition Challenge at Nodeflux as an AI Engineer Intern</b>

Steps to use:
- Download the dataset for Processed IndoCeleb training + benchmark data (LFW and CFP_FP) at [Google Drive](https://drive.google.com/file/d/1ngygrOljr8b5DIs-h8AHCMxVR1DLcW-v/view?usp=sharing)
- Put the data_train folder from the zip at the data folder
- If you want to fine-tune from a pretrained model, make sure to download the model in the link then put the model into model/pretrained.pth and configure your cfg file at config/config.py
- Run the Training.ipynb file


If you use any of the code provided in this repository, please cite the following paper:
## Citation
```
@misc{boutros2021elasticface,
      title={ElasticFace: Elastic Margin Loss for Deep Face Recognition}, 
      author={Fadi Boutros and Naser Damer and Florian Kirchbuchner and Arjan Kuijper},
      year={2021},
      eprint={2109.09416},
      archivePrefix={arXiv},
      primaryClass={cs.CV}
}

```

## License

```
This project is licensed under the terms of the Attribution-NonCommercial-ShareAlike 4.0 
International (CC BY-NC-SA 4.0) license. 
Copyright (c) 2021 Fraunhofer Institute for Computer Graphics Research IGD Darmstadt
```
